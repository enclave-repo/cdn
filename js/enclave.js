/* Calendario */
let playlistmanagerwrap = document.getElementById('playlistmanagerwrap');
const url = 'https://enclave.cyou';

let iframe = document.createElement('iframe');
iframe.setAttribute('src', url);
iframe.setAttribute('width', '96%');
iframe.setAttribute('height', '510px');
iframe.setAttribute('scrolling', 'auto');
iframe.setAttribute('frameborder', '0');
iframe.setAttribute('id', 'iframeCalendar');
iframe.style.marginLeft = '15px';
iframe.style.display = 'none';
playlistmanagerwrap.after(iframe);

let emoteListBtn = document.getElementById('emotelistbtn');

let toogleCalendarBtn = document.createElement('button');
toogleCalendarBtn.classList.add('btn');
toogleCalendarBtn.classList.add('btn-sm');
toogleCalendarBtn.classList.add('btn-default');
toogleCalendarBtn.innerHTML = 'Calendario';
toogleCalendarBtn.onclick = function () {

let cal = document.getElementById('iframeCalendar');
const display = cal.style.display === 'none' ? 'block' : 'none';

if (display === 'block') {
    cal.src += '';
    setTimeout(function () {        
        cal.style.display = display;
    }, 500);
} else 
    cal.style.display = display;
}

emoteListBtn.after(toogleCalendarBtn);
